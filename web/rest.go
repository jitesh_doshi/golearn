package main

import (
	"github.com/ant0ine/go-json-rest/rest"
)

// The ResouceHandler that should be setup as the main handler for all HTTP calls in main.
var WebRest struct {
	Handler rest.ResourceHandler
	Routes  []*rest.Route
}

// Simple type for sending string messages as REST responses
type Message struct {
	Body string
}

// simple hello function REST http handler
func hello(w rest.ResponseWriter, req *rest.Request) {
	w.WriteJson(&Message{Body: "Hello World!"})
}

// init function - sets up the routes
func init() {
	WebRest.Routes = append(WebRest.Routes, &rest.Route{"GET", "/hello", hello})
	WebRest.Handler.SetRoutes(WebRest.Routes...)
}
