package main

import (
	"github.com/ant0ine/go-json-rest/rest"
	"github.com/jinzhu/gorm"
	"net/http"
	"reflect"
	"strconv"
)

func SetupCrudRoutes(name string, s interface{}) {
	ORM.AutoMigrate(s)     // create table if missing
	t := reflect.TypeOf(s) // 't' is reflection.Type of 's'
	base := "/" + name
	listfunc := func(w rest.ResponseWriter, req *rest.Request) {
		rs := reflect.New(reflect.SliceOf(t)).Interface()
		if err := ORM.Find(rs).Error; err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.WriteJson(rs)
	}
	getfunc := func(w rest.ResponseWriter, req *rest.Request) {
		id, err := strconv.Atoi(req.PathParam("id"))
		if err != nil {
			rest.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		r := reflect.New(t).Interface()
		err = ORM.Find(r, id).Error
		switch err {
		case gorm.RecordNotFound:
			rest.Error(w, err.Error(), http.StatusNotFound)
			return
		case nil:
			w.WriteJson(r)
			return
		default:
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	postfunc := func(w rest.ResponseWriter, req *rest.Request) {
		r := reflect.New(t).Interface()
		if err := req.DecodeJsonPayload(r); err != nil {
			rest.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if err := ORM.Create(r).Error; err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	putfunc := func(w rest.ResponseWriter, req *rest.Request) {
		r := reflect.New(t).Interface()
		if err := req.DecodeJsonPayload(r); err != nil {
			rest.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if err := ORM.Save(r).Error; err != nil {
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	deletefunc := func(w rest.ResponseWriter, req *rest.Request) {
		id, err := strconv.Atoi(req.PathParam("id"))
		if err != nil {
			rest.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		r := reflect.New(t).Interface()
		err = ORM.Delete(r, id).Error
		switch err {
		case gorm.RecordNotFound:
			rest.Error(w, err.Error(), http.StatusNotFound)
			return
		case nil:
			return
		default:
			rest.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	WebRest.Routes = append(WebRest.Routes,
		&rest.Route{"GET", base, listfunc},
		&rest.Route{"GET", base + "/:id", getfunc},
		&rest.Route{"POST", base, postfunc},
		&rest.Route{"PUT", base, putfunc},
		&rest.Route{"DELETE", base + "/:id", deletefunc},
	)
}
