package main

import "time"

type Product struct {
	Id        int64
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func init() {
	SetupCrudRoutes("product", Product{})
}
