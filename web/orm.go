package main

import (
	"flag"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

var ORM gorm.DB
var db_type = flag.String("db_type", "mysql", "DB type")
var db_string = flag.String("db_string", "root:@/test?charset=utf8&parseTime=True", "DB connection string")

func init() {
	var err error
	flag.Parse() // must call this after all flags are defined
	if ORM, err = gorm.Open(*db_type, *db_string); err != nil {
		panic(err)
	}
	ORM.SingularTable(true) // table names derived from type names should be singular
}
