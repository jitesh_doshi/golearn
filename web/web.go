package main

import (
	"flag"
	"log"
	"net/http"
)

var addr = flag.String("addr", ":8080", "Internet address to listen on")

func main() {
	flag.Parse() // must call this after all flags are defined
	log.Printf("Listening on %s\n", *addr)
	http.ListenAndServe(*addr, &WebRest.Handler)
}
