package main

import "time"

type Person struct {
	Id        int64
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
}

// init function - sets up the routes
func init() {
	SetupCrudRoutes("person", Person{})
}
