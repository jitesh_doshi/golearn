package main

import (
	"errors"
	"golearn/plugins"
)

func GetInfo() *plugins.PluginInfo {
	return &plugins.PluginInfo{
		ID:           "example2",
		Version:      "0.0.1",
		Name:         "Example Two",
		Description:  "An example plugin. Second one.",
		Dependencies: []string{"example1"},
		Init: func() error {
			return errors.New("Foo error")
		},
	}
}
