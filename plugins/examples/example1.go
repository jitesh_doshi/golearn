package main

import (
	"golearn/plugins"
)

func GetInfo() *plugins.PluginInfo {
	return &plugins.PluginInfo{
		ID:           "example1",
		Version:      "0.0.1",
		Name:         "Example One",
		Description:  "An example plugin. First one.",
		Dependencies: []string{},
	}
}
