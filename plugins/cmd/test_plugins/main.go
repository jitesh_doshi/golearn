package main

import (
	"golearn/plugins"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatalf("Usage: %s path/to/plugin.so\n", os.Args[0])
	}
	path := os.Args[1]
	if pi, err := plugins.LoadPlugin(path); err != nil {
		log.Fatalln(err)
	} else {
		log.Printf("PluginInfo: %#v\n", pi)
		log.Printf("pi.Init: %#v\n", pi.Init)
		if pi.Init != nil {
			log.Println("Calling init")
			if err := pi.Init(); err != nil {
				log.Fatalf("Plugin (%s) Init error: %s\n", pi.Name, err)
			}
		} else {
			log.Println("No init")
		}
	}
}
