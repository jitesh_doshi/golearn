package plugins

import (
	"errors"
	"plugin"
)

type PluginInfo struct {
	ID           string         // machine-name or ID of the plugin (must be unique)
	Version      string         // plugin version (SemVer)
	Name         string         // human-readble name of the plugin
	Description  string         // describes the purpose of the plugin and functionality
	Dependencies []string       // IDs of plugins this one depends on (requires)
	Init         (func() error) // Initiazes this module (before anyone can use it)
}

type Initable interface {
	Init() error
}

func LoadPlugin(path string) (*PluginInfo, error) {
	p, err := plugin.Open(path)
	if err != nil {
		return nil, err
	}
	sym, err := p.Lookup("GetInfo")
	if err != nil {
		return nil, err
	}

	load, ok := sym.(func() *PluginInfo)
	if !ok {
		return nil, errors.New(`Symbol "PluginInfo" not of type (PluginInfo)`)
	}
	return load(), nil
}
